import Data.Char
import System.IO
import System.Environment
import Data.Matrix

--Necesita instalarse el Data.Matrix
main = (do
 args <- getArgs

 let 
  versionDB = args !! 0
  testVersion = args !! 1
  
 handleDB <- openFile ("DB"++versionDB++".txt") ReadMode
 db <- hGetContents handleDB
 
 let
  dbMatrix = map (difusion.getMatrixFromLine) (lines db)
 handleLabels <- openFile ("labels"++versionDB++".txt") ReadMode
 labels <- hGetContents handleLabels
 
 handleTest <- openFile ("dbTest"++testVersion++".txt") ReadMode
 numbersTest <- hGetContents handleTest
 
 let
  digitsTest = map (difusion.getMatrixFromLine) (lines numbersTest)
  evaluate = map (predictor dbMatrix labels) digitsTest

 print evaluate
 
 handleLabelsTest <- openFile ("labelsT"++testVersion++".txt") ReadMode
 labelsTest <- hGetContents handleLabelsTest
 let
  digitsToEval = map digitToInt labelsTest
  listDigitAndGT = zip evaluate digitsToEval
  listHits = map equalsPair listDigitAndGT
  hits = sum listHits
 print "hits totales: "
 print hits
 )

--- === constantes auxiliares === ---
cota = 2000
sizeMatrix = 28
valueMult = 28
arrayIndex = zero 1 10

equalsPair :: (Int,Int) -> Int
equalsPair (a,b) = if a==b then 1 else 0

--Filtra el listado de distancias obtenidos luego de agruparlos con cada labels correspondiente, para identificar aquel de mayor frecuencia
predictor :: [Matrix Int] -> [Char] -> Matrix Int -> Int
predictor dbMatrix labels digitTest = mayorFrecuencia filterList
 where distances = getDistanceToMatrixBD dbMatrix digitTest
       zipListDL = zip distances labels 
       filterList = filter ((<=cota).fst) zipListDL

--calcula la frecuencia de cada digito con arrayIndex como un array base inicial de 10 posiciones, para luego obtener el maximo con la cabeza como maximo inicial
mayorFrecuencia :: [(Float,Char)] -> Int
mayorFrecuencia listaTuplas = posOfMaxFromTuplaList listWithPositions maxBase
 where listaDeFrecuencia = frequencyList listaTuplas arrayIndex
       listWithPositions = zip (toList listaDeFrecuencia) [0..9]
       maxBase = head listWithPositions

--calcula la frecuencia de cada digito con un arrayIndex como base, se utiliza matrizCount como el array.
frequencyList :: [(Float,Char)] -> Matrix Int -> Matrix Int
frequencyList [] matrizCount = matrizCount
frequencyList [x] matrizCount = setElem value posInMatrix matrizCount
 where digit = (digitToInt.snd) x
       posInMatrix = (1,(digit+1))
       value = ((!) matrizCount posInMatrix) + 1
frequencyList (x:xs) matrizCount = frequencyList xs (setElem value posInMatrix matrizCount) 
 where digit = (digitToInt.snd) x
       posInMatrix = (1,(digit+1))
       value = ((!) matrizCount posInMatrix) + 1

-- Obtiene la posicion de la tupla con la maxima Frecuencia .. utiliza un parametro de maxima (Frecuencia,Digito) como base
posOfMaxFromTuplaList :: [(Int,Int)] -> (Int,Int) -> Int
posOfMaxFromTuplaList [x] (maxFreq,maxDig) = 
 if (fst x) > maxFreq 
 then (snd x) 
 else maxDig
posOfMaxFromTuplaList (x:xs) (maxFreq,dig) = 
 if (fst x) > maxFreq 
 then posOfMaxFromTuplaList xs x 
 else posOfMaxFromTuplaList xs (maxFreq,dig)
 
-- Dado un string de lista de numeros "[0,1,0,0,1,0,1,0,..]", los devuelve como una matriz cuadrada de 0's y 1's de tamaño sizeMatrix.
getMatrixFromLine nroCodificado = fromList sizeMatrix sizeMatrix (map ((*255).digitToInt) nroCodificado)

difusion :: Matrix Int -> Matrix Int
difusion matrix = mapPos (newValue matrix) matrix

newValue matrix (i,j) a = 
  if a == 255 then a else valueMult * (sumBlancos (i,j)) matrix

--obtiene la submatriz con indices corregidos viendo si es blanco los valores de alrededor
sumBlancos :: (Int,Int)-> Matrix Int -> Int
sumBlancos (i,j) matrix = 
 sum (mapPos esBlanco entorno) 
   where  corrI = fitIndex i (nrows matrix)
          corrJ = fitIndex j (ncols matrix)
          botI = fst corrI
          topI = snd corrI
          botJ = fst corrJ
          topJ = snd corrJ
          entorno = submatrix botI topI botJ topJ matrix

--devuelve el min y max que puede tomar el eje i-esimo de entre .. i-1, i, i+1
fitIndex ind limit
 | (ind-1)>=1 && (ind+1)<=limit = (ind-1,ind+1)
 | (ind-1)>=1 && (ind+1)>limit = (ind-1,ind)
 | (ind-1)<1 && (ind+1)<=limit = (ind,ind+1)
 | otherwise = (ind,ind)  
-- | (i-1)<1 && (i+1)>nrow = (i,i) para J seria lo mismo con los parametros j ncol 

esBlanco :: (Int,Int) -> Int -> Int
esBlanco _ a = if a==255 then 1 else 0

--- === métodos para el cálculo de distancia === ---

-- Obtiene las distancias a la base de datos de matrices 'xs' con una imagen 'y'.. 
getDistanceToMatrixBD :: [Matrix Int] -> Matrix Int -> [Float]  
getDistanceToMatrixBD dbMatrix matrixInput = map (distEuclMatrix matrixInput) dbMatrix
-- Distancia euclideana entre dos matrices
distEuclMatrix :: Matrix Int -> Matrix Int -> Float
distEuclMatrix a b = (sqrt.fromIntegral) ( sum (diffCuadr (toList a) (toList b) ) )

--Dadas dos listas [a1,a2,..] [b1,b2,..] retorna [(a1-b1)^2,(a2-b2)^2,..]
diffCuadr :: [Int] -> [Int] -> [Int]
diffCuadr xs ys = map unitDiffCuad (zip xs ys)

unitDiffCuad :: (Int,Int) -> Int
unitDiffCuad (x,y) = (x-y)^2