Ejecutar o compilar el programa con 2 parametros, el primero indica la versión del dataset base y el segundo la versión del dataset de prueba.

Datasets base disponibles:
DB1 - 2000 Entradas
DB2 - 2000 Entradas
DB3 - 2000 Entradas
DB4 - 2000 Entradas
DB5 - 2000 Entradas
DBTotal - 10000 Entradas

Datasets de prueba disponibles:
dbTest1 - 20 Dígitos de Prueba
dbTest1000 - 1000 Dígitos de Prueba
dbTest2000 - 1000 Dígitos de Prueba
dbTest3000 - 1000 Dígitos de Prueba

Compilación rapida:
Ubicado en la carpeta con todos los archivos ejecutar:
runhaskell Reconocedor_Digitos.hs <Version-Dataset-Base> <Version-Dataset-Prueba>

Por ejemplo:
runhaskell Reconocedor_Digitos.hs 4 1000 <- Utiliza Dataset-Base 4 y Dataset de Pruebas 1000
runhaskell Reconocedor_Digitos.hs Total 2000 <- Utiliza Dataset-Base Total y Dataset de Pruebas 2000
